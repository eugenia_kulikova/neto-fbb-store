module.exports=function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify:{
            options:{
                manage:false,
            },
            my_target:{
                files:{
                    'scripts/main.min.js':['js/input1.js','js/input2.js']
                }
            }
        },
        sass:{
           dist:{
               files:{
                   'css/style.css':'sass/style.scss'
               }
           }
        },
        cssmin: {
            my_target: {
                files: [{
                    expand:true,
                    cwd:'css/',
                    src:['*.css','!*.min.css'],
                    dest:'css/',
                    ext:'.min.css'
                }]
            }
        },
        watch: {
            sass: {
                files: ['sass/*.scss'],
                tasks: ['sass', 'cssmin'],
            }
        },
        imagemin: {
            options: {
                optimizationLevel: 3,
                progressive: true,
                interlaced: true,
                pngquant: true
            },
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'images/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'images/'
                }]
            },
            ftp_push: {
                demo: {
                    options: {
                        authKey: 'netology',
                        host: 'university.netology.ru',
                        dest: '/fbb-store/',
                        port: 21
                    },
                    files: [{
                        expand: true,
                        cwd: '.',
                        src: [
                            'index.html',
                            'css/main.css'
                        ]
                    }]
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('bootstrap');
    grunt.loadNpmTasks('jquery');
    grunt.loadNpmTasks('jquery-ui');
    grunt.loadNpmTasks('grunt-ftp-push');

    grunt.registerTask('default', ['sass', 'ftp_push']);
};

