var GB=[];
var myvar;
var money=[];
var book = {};
var books = [];
var k=0;
$(document).ready(function () {


    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://netology-fbb-store-api.herokuapp.com/book', false);
    xhr.send();
    if (xhr.status != 200) {
        alert(xhr.status + ': ' + xhr.statusText);
    } else {
        JSON.parse(xhr.responseText).forEach(function (n, v) {
            books.push(n);
            GB.push(n);
        });
    }
    // пригорошня книг
    fourB();

    // при клике на книге переходим в инфу
    $(infoAboutBook());

    // чуть больше пригорошни книг
    adfourBook();

    // самая короткая
    $("#sortable").sortable({revert: true});

    $(du());
    $(".logo,.logo_bottom").click(function () {
        $(".main").load("index.html .main", function (response, status, xhr) {
            if (status == "success") {
                k=0;
                $(fourB());
                adfourBook();
            }
        });
    });
});

function infoAboutBook() {
    $(".book_list").dblclick(function (ed) {
        myvar=ed.target.getAttribute("id");
        $(".main").load("preview.html",function (response, status, xhr) {
            if(status=="success")
                {
                        var strb='<img  src="'+getbookbyId(myvar).cover.large+'">';
                        $("div.cover_preview").append(strb);
                        $(".buy_button")[0].value=("Купить за жалкие" +getbookbyId(myvar).price.toFixed(2));
        // загрузка цен
                        var xhr1 = new XMLHttpRequest();
                        xhr1.open('GET', 'https://netology-fbb-store-api.herokuapp.com/currency', false);
                        xhr1.send();
                    if (xhr1.status != 200) {
                        alert( xhr1.status + ': ' + xhr1.statusText );
                    } else{
                                JSON.parse(xhr1.responseText).forEach(function (n) {
                                    money.push(n);
                                });
                                default_value(money)
                        }
                }
            // при смене валюты
                    $('#select1').change(function () {
                    postloadp(money[$("#select1 option:selected").val()].ID);
                    });
            // переход на покупку
                    $(order());
            });
        });
}
// покупка
function order() {
    $('.buy_button').click(function () {
    $(".main").load("order.html",function (response, status, xhr) {
        if(status=="success")
        {

        }
    });
});
}
// изменение стилей при перетаскивании
function du() {
    $('.book_cover').mousedown(function (event, n) {

        $(event.currentTarget).css({'cursor': 'pointer', 'box-shadow': ' 0.5em 0.5em 10px rgba(20, 9, 49, 0.5)'});
        $(event.currentTarget.children).css({'width': '250px', 'height': '360px'});
    });
    $('div.book_cover').mouseup(function (e) {

        $(e.currentTarget).css({'cursor': 'auto', 'box-shadow': ' 0.5em 0.5em 5px rgba(122, 122, 122, 0.5)'});
        $(event.currentTarget.children).css({'width': '210px', 'height': '300px'});
    });
}

function getbookbyId(idd) {
    var tempb;
    GB.forEach(function (v) {
        if(idd==v.id) {
            tempb=v;
        }
    });
    return tempb;
}

function getcuorsebyId(idd) {
    var tempp;
    money.forEach(function (v) {
        if(idd==v.ID) {
            tempp=v.Value;
        }
    });
    return tempp;
}
// обновление цены на
function postloadp(a) {
    $(".buy_button")[0].value=("Купить за жалкие "+(getcuorsebyId(getbookbyId(myvar).currency)/getcuorsebyId(a)*getbookbyId(myvar).price).toFixed(2));
}
// заполнение инфы о валютах
function default_value(data) {
    data.forEach(function(v, n) {
        $('#select1').append('<option value="' + n + '">' + v.Name + '</option>');
    });
    $("#select1 [value='9']").attr("selected", "selected");
};

function fourB() {
    for(var i=0;i<4;i++)
    {
        book=books[k];
        $('.book_cover:eq('+i+')').append('<img id="'+book.id+'" src="'+book.cover.small+'">');
        $('.description:eq('+i+')').append(book.info);
        k++;
    }
}
function adfourBook() {
    $('#morebooks').click(function () {
        for (var i = 0; i < 4; i++) {
            book = books[k];
            if (book == null) {
                $('.more_books').remove();
            }
            else $('.row .more_books').before('<div class="ui-state-default col-lg-3 col-md-4 col-sm-6 col-xs-12 text-center col-xs-offset-0 book_list"><div class="book_cover img-responsive"><img  id="' + book.id + '" src="' + book.cover.small + '"></div><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span><p class="description ">' + book.info + '</p></div>');
            {$(infoAboutBook());}
            k++;
        }
        $(du());
    });
}
